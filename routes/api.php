<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Auth
Route::post('v1/login' , 'Auth\LoginController@login');
Route::post('v1/register' , 'Auth\RegisterController@register');

Route::apiResources(['users' => 'Auth\UserAccountController']);
Route::apiResources(['role' => 'Auth\RoleController']);
Route::apiResources(['userProfile' => 'Auth\UserProfileController']);
Route::post('updateUserProfile' , 'Auth\UserProfileController@updateUserProfile');

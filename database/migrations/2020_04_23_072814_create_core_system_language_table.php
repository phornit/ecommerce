<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreSystemLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_system_language', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('trans_name');
            $table->string('localization');
            $table->string('script');
            $table->string('image');
            $table->integer('ordering');
            $table->boolean('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_system_language');
    }
}

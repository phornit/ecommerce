<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class sendCollection extends ResourceCollection
{
    private $statusCode;
    private $message;

    public function __construct($collection, $message = 'Success', $statusCode = 200)
    {
        parent::__construct($collection);

        if ($collection->isEmpty()){
            $this->statusCode = 404;
            $this->message = 'Record not found';
        }else{
            $this->statusCode = $statusCode;
            $this->message = $message;
        }
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request){
        return [
            'statusCode' => $this->statusCode,
            'message' => $this->message
        ];
    }
}

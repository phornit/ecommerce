<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Http\Resources\sendResource;
use App\Libraries\CoreFunction;
use Closure;

class Authkey extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = CoreFunction::getApiKey();
        $token = $request->header('X-API-KEY');
        if ($token != $key){
            return $this->sendInvalidApiKeyResponse();
        }
        return $next($request);
    }
}

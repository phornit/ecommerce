<?php

namespace App\Http\Controllers;

use App\Http\Resources\sendCollection;
use App\Http\Resources\sendResource;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendListResponse($data)
    {
        return new sendCollection($data);
    }

    public function sendResponse($data, $statusCode = 200 , $message = 'Success')
    {
        if (empty($data)){
            $statusCode = 404;
            $message = 'Record not found';
        }

        $response = [
            'data'    =>  $data ,
            'statusCode' => $statusCode,
            'message' => $message,
        ];
        return response()->json($response, $statusCode);
    }

    public function sendNotFoundResponse($message = 'Record Not Found')
    {
        $response = [
            'data'    =>  null ,
            'statusCode' => 404,
            'message' => $message,
        ];
        return response()->json($response, 404);
    }

    public function sendInvalidResponse($message = 'Invalid request')
    {
        $response = [
            'data'    =>  null ,
            'statusCode' => 404,
            'message' => $message,
        ];
        return response()->json($response, 400);
    }

    public function sendInvalidApiKeyResponse($message = 'Invalid API Key')
    {
        $response = [
            'data'    =>  null ,
            'statusCode' => 404,
            'message' => $message,
        ];
        return response()->json($response, 401);
    }

    public function sendError($error, $errorMessages = [], $code = 404){
        $response = [
            'data' => null,
            'success' => false,
            'statusCode' => 404,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['error_messages'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
